# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cameras.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
import os
import configparser
import time
import json
from mqtt import *
from pynq.overlays.base import BaseOverlay
from pynq.lib import LED, Switch, Button

class Ui_Cameras(object):
    def setupUi(self, Cameras):
        Cameras.setObjectName("MainWindow")
        Cameras.resize(504, 287)
        self.centralwidget = QtWidgets.QWidget(Cameras)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 2, 8, 1, 1)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.lineEdit_3.returnPressed.connect(self.rightCamera)
        self.gridLayout.addWidget(self.lineEdit_3, 6, 4, 1, 2)
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(6)
        self.pushButton_5.setFont(font)
        self.pushButton_5.setObjectName("pushButton_5")
        self.gridLayout.addWidget(self.pushButton_5, 9, 2, 1, 1)
        self.pushButton_10 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_10.setCheckable(True)
        self.pushButton_10.setChecked(True)
        if not os.path.exists("cameras.ini"):
            print("Cameras turned on")
            print("LED 0 turning on")
            try:
                base = BaseOverlay("base.bit")
                led0 = base.leds[0]
                led0.on()
            except Exception as e:
                pass

            config = configparser.RawConfigParser()

            f = open("cameras.ini", "w")
            config.add_section("Cameras")
            config.set("Cameras", "status", "on")
            config.write(f)
        else:
            pass
        self.pushButton_10.clicked.connect(self.on)
        self.pushButton_10.setObjectName("pushButton_10")
        self.gridLayout.addWidget(self.pushButton_10, 0, 0, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setObjectName("label_11")
        self.gridLayout.addWidget(self.label_11, 8, 3, 1, 2)
        self.lineEdit_5 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.lineEdit_5.returnPressed.connect(self.widthROI)
        self.gridLayout.addWidget(self.lineEdit_5, 9, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 4, 4, 1, 2)
        self.lineEdit_8 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.lineEdit_8.returnPressed.connect(self.delay)
        self.gridLayout.addWidget(self.lineEdit_8, 3, 6, 1, 2)
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 9, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.returnPressed.connect(self.exposureTime)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 2, 6, 1, 2)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 8, 0, 1, 1)
        self.lineEdit_6 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.lineEdit_6.returnPressed.connect(self.leftROI)
        self.gridLayout.addWidget(self.lineEdit_6, 8, 5, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 5, 0, 1, 1)
        self.pushButton_9 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_9.setObjectName("pushButton_9")
        self.gridLayout.addWidget(self.pushButton_9, 3, 8, 1, 1)
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(6)
        self.pushButton_6.setFont(font)
        self.pushButton_6.setObjectName("pushButton_6")
        self.gridLayout.addWidget(self.pushButton_6, 8, 7, 1, 1)
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(6)
        self.pushButton_4.setFont(font)
        self.pushButton_4.setObjectName("pushButton_4")
        self.gridLayout.addWidget(self.pushButton_4, 8, 2, 1, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(6)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout.addWidget(self.pushButton_2, 6, 2, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        self.label_12.setObjectName("label_12")
        self.gridLayout.addWidget(self.label_12, 9, 3, 1, 2)
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(6)
        self.pushButton_3.setFont(font)
        self.pushButton_3.setObjectName("pushButton_3")
        self.gridLayout.addWidget(self.pushButton_3, 6, 6, 1, 2)
        self.horizontalSlider = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider.setMaximum(5000)
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")
        self.gridLayout.addWidget(self.horizontalSlider, 2, 0, 1, 6)
        self.label = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 2)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 6, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 3, 0, 1, 2)
        self.comboBox_2 = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_2.setObjectName("comboBox_2")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.gridLayout.addWidget(self.comboBox_2, 5, 4, 1, 3)
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.gridLayout.addWidget(self.comboBox, 5, 1, 1, 2)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 7, 1, 1, 1)
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(6)
        self.pushButton_7.setFont(font)
        self.pushButton_7.setObjectName("pushButton_7")
        self.gridLayout.addWidget(self.pushButton_7, 9, 7, 1, 1)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setText("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.lineEdit_2.returnPressed.connect(self.leftCamera)
        self.gridLayout.addWidget(self.lineEdit_2, 6, 1, 1, 1)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.lineEdit_4.returnPressed.connect(self.topROI)
        self.gridLayout.addWidget(self.lineEdit_4, 8, 1, 1, 1)
        self.lineEdit_7 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.lineEdit_7.returnPressed.connect(self.heightROI)
        self.gridLayout.addWidget(self.lineEdit_7, 9, 5, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 4, 1, 1, 1)
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setObjectName("pushButton_8")
        self.pushButton_8.clicked.connect(self.off)
        self.gridLayout.addWidget(self.pushButton_8, 0, 1, 1, 1)
        Cameras.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(Cameras)
        self.statusbar.setObjectName("statusbar")
        Cameras.setStatusBar(self.statusbar)
        self.retranslateUi(Cameras)
        QtCore.QMetaObject.connectSlotsByName(Cameras)

    def exposureTime(self):
        if os.path.exists("cameras.ini"):
            textboxValue = self.lineEdit.text()
            if self.lineEdit.text() == "":
                self.horizontalSlider.setValue(0)
            else:
                self.horizontalSlider.setValue(int(textboxValue))
            
            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "exposure time": textboxValue, "cmd": "set exposure time of cameras"}}, indent=2))
            time.sleep(1)
            client.loop_stop()

            print("Exposure time: " + textboxValue + "ms")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("Exposure")
            except Exception as e:
                pass
            try:
                config.set("Exposure", "time", textboxValue)
            except Exception as e:
                pass
            config.write(f)

    def delay(self):
        if os.path.exists("cameras.ini"):
            textboxValue2 = self.lineEdit_8.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "delay": textboxValue2, "cmd": "set delay of cameras"}}, indent=2))
            time.sleep(1)
            client.loop_stop()

            print("Delay: " + textboxValue2 + "ms")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("Delay")
            except Exception as e:
                pass
            try:
                config.set("Delay", "time", textboxValue2)
            except Exception as e:
                pass
            config.write(f)

    def leftCamera(self):
        if os.path.exists("cameras.ini"):
            leftMode = self.comboBox.currentText()
            textboxValue3 = self.lineEdit_2.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "left camera mode": leftMode, "left camera lines": textboxValue3 + "px", "cmd": "set left camera mode and left camera lines"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Left camera mode: " + leftMode)
            print("Left camera lines: " + textboxValue3 + "px")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("Left camera")
            except Exception as e:
                pass
            try:
                config.set("Left camera", "mode", leftMode)
            except Exception as e:
                pass
            try:
                config.set("Left camera", "lines", textboxValue3)
            except Exception as e:
                pass
            config.write(f)             

    def rightCamera(self):
        if os.path.exists("cameras.ini"):
            rightMode = self.comboBox_2.currentText()
            textboxValue4 = self.lineEdit_3.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "right camera mode": rightMode, "right camera lines": textboxValue4 + "px", "cmd": "set right camera mode and right camera lines"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Right camera mode: " + rightMode)
            print("Right camera lines: " + textboxValue4 + "px")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("Right camera")
            except Exception as e:
                pass
            try:
                config.set("Right camera", "Mode", rightMode)
            except Exception as e:
                pass
            try:
                config.set("Right camera", "Lines", textboxValue4)
            except Exception as e:
                pass
            config.write(f)

    def topROI(self):
        if os.path.exists("cameras.ini"):
            textboxValue5 = self.lineEdit_4.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "top ROI": textboxValue5 + "px", "cmd": "set top ROI"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Top ROI: " + textboxValue5 + "px")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("ROI")
            except Exception as e:
                pass
            try:
                config.set("ROI", "top", textboxValue5)
            except Exception as e:
                pass
            config.write(f)

    def leftROI(self):
        if os.path.exists("cameras.ini"):
            textboxValue6 = self.lineEdit_6.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "left ROI": textboxValue6 + "px", "cmd": "set left ROI"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Left ROI: " + textboxValue6 + "px")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("ROI")
            except Exception as e:
                pass
            try:
                config.set("ROI", "left", textboxValue6)
            except Exception as e:
                pass
            config.write(f)

    def widthROI(self):
        if os.path.exists("cameras.ini"):
            textboxValue7 = self.lineEdit_5.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "width ROI": textboxValue7 + "px", "cmd": "set width ROI"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Width ROI: " + textboxValue7 + "px")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("Width ROI")
            except Exception as e:
                pass
            try:
                config.set("ROI", "width", textboxValue7)
            except Exception as e:
                pass
            config.write(f)

    def heightROI(self):
        if os.path.exists("cameras.ini"):
            textboxValue8 = self.lineEdit_7.text()

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "height ROI": textboxValue8 + "px", "cmd": "set height ROI"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Height ROI: " + textboxValue8 + "px")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("ROI")
            except Exception as e:
                pass
            try:
                config.set("ROI", "height", textboxValue8)
            except Exception as e:
                pass
            config.write(f)

    def on(self):            
        if os.path.exists("cameras.ini"):
            config = configparser.RawConfigParser()
            config.read("cameras.ini")
            
            if self.pushButton_10.isChecked():
                client = device()
                client.run()

                client.loop_start()
                print("\n" + "Connected to broker")
                time.sleep(1)
                print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
                client.subscribe("microscope/light_sheet_microscope/cameras/UI")
                print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
                client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "cmd": "cameras turning on"}}, indent=2))
                time.sleep(1)
                client.loop_stop()
                print("Cameras turned on")
                self.pushButton_8.clicked.connect(self.off)
                self.pushButton_10.setCheckable(True)
                self.pushButton_10.setChecked(True)
                try:
                    self.horizontalSlider.setValue(config.getint("Exposure", "time"))
                    print("Exposure time set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit.setText(str(config.getint("Exposure", "time")))
                    print("Exposure time textbox set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_8.setText(str(config.getint("Delay", "time")))
                    print("Delay time textbox set")
                except Exception as e:
                    pass
                try:
                    self.comboBox.setCurrentText(config.get("Left camera", "mode"))
                    print("Left camera mode set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_2.setText(config.get("Left camera", "lines"))
                    print("Left camera lines set")
                except Exception as e:
                    pass
                try:
                    self.comboBox_2.setCurrentText(config.get("Right camera", "mode"))
                    print("Right camera mode set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_3.setText(config.get("Right camera", "lines"))
                    print("Right camera lines set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_4.setText(config.get("ROI", "top"))
                    print("Top ROI set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_6.setText(config.get("ROI", "left"))
                    print("Left ROI set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_5.setText(config.get("ROI", "width"))
                    print("Width ROI set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_7.setText(config.get("ROI", "height"))
                    print("Height ROI set")
                except Exception as e:
                    pass
                self.lineEdit.returnPressed.connect(self.exposureTime)
                self.lineEdit_8.returnPressed.connect(self.delay)
                self.lineEdit_2.returnPressed.connect(self.leftCamera)
                self.lineEdit_3.returnPressed.connect(self.rightCamera)
                self.lineEdit_4.returnPressed.connect(self.topROI)
                self.lineEdit_5.returnPressed.connect(self.widthROI)
                self.lineEdit_6.returnPressed.connect(self.leftROI)
                self.lineEdit_7.returnPressed.connect(self.heightROI)               

                f = open("cameras.ini", "w")
                try:
                    config.add_section("Cameras")
                except Exception as e:
                    pass
                try:
                    config.set("Cameras", "status", "on")
                except Exception as e:
                    pass
                config.write(f)

    def off(self):
        if os.path.exists("cameras.ini"):
            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.subscribe("microscope/light_sheet_microscope/cameras/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/cameras/UI")
            client.publish("microscope/light_sheet_microscope/cameras/UI", json.dumps({"type": "device", "payload":{"name": "cameras", "cmd": "device turning off"}}, indent=2))
            time.sleep(1)
            client.loop_stop()

            print("Cameras turning off")

            config = configparser.RawConfigParser()
            config.read("cameras.ini")

            f = open("cameras.ini", "w")
            try:
                config.add_section("Cameras")
            except Exception as e:
                pass
            try:
                config.set("Cameras", "status", "off")
            except Exception as e:
                pass
            config.write(f)        

            self.pushButton_8.clicked.disconnect(self.off)
            self.pushButton_10.setChecked(False)
            self.lineEdit.clear()
            self.horizontalSlider.setValue(0)
            self.lineEdit.returnPressed.disconnect(self.exposureTime)
            self.lineEdit_8.clear()
            self.lineEdit_8.returnPressed.disconnect(self.delay)
            self.lineEdit_2.clear()
            self.lineEdit_2.returnPressed.disconnect(self.leftCamera)
            self.lineEdit_3.clear()
            self.lineEdit_3.returnPressed.disconnect(self.rightCamera)
            self.lineEdit_4.clear()
            self.lineEdit_4.returnPressed.disconnect(self.topROI)
            self.lineEdit_5.clear()
            self.lineEdit_5.returnPressed.disconnect(self.widthROI)
            self.lineEdit_6.clear()
            self.lineEdit_6.returnPressed.disconnect(self.leftROI)
            self.lineEdit_7.clear()
            self.lineEdit_7.returnPressed.disconnect(self.heightROI)

    def retranslateUi(self, Cameras):
        _translate = QtCore.QCoreApplication.translate
        Cameras.setWindowTitle(_translate("Cameras", "MainWindow"))
        self.pushButton.setText(_translate("Cameras", "ms"))
        self.pushButton_5.setText(_translate("Cameras", "px"))
        self.pushButton_10.setText(_translate("Cameras", "ON"))
        self.label_11.setText(_translate("Cameras", "Left"))
        self.label_6.setText(_translate("Cameras", "Right"))
        self.label_10.setText(_translate("Cameras", "Width"))
        self.label_9.setText(_translate("Cameras", "Top"))
        self.label_3.setText(_translate("Cameras", "Mode"))
        self.pushButton_9.setText(_translate("Cameras", "ms"))
        self.pushButton_6.setText(_translate("Cameras", "px"))
        self.pushButton_4.setText(_translate("Cameras", "px"))
        self.pushButton_2.setText(_translate("Cameras", "px"))
        self.label_12.setText(_translate("Cameras", "Height"))
        self.pushButton_3.setText(_translate("Cameras", "px"))
        self.label.setText(_translate("Cameras", "Exposure Time"))
        self.label_7.setText(_translate("Cameras", "Lines"))
        self.label_2.setText(_translate("Cameras", "Delay"))
        self.comboBox_2.setItemText(0, _translate("Cameras", "Program"))
        self.comboBox_2.setItemText(1, _translate("Cameras", "Shutter Priority"))
        self.comboBox_2.setItemText(2, _translate("Cameras", "Aperture Priority"))
        self.comboBox_2.setItemText(3, _translate("Cameras", "Manual"))
        self.comboBox.setItemText(0, _translate("Cameras", "Program"))
        self.comboBox.setItemText(1, _translate("Cameras", "Shutter Priority"))
        self.comboBox.setItemText(2, _translate("Cameras", "Aperture Priority"))
        self.comboBox.setItemText(3, _translate("Cameras", "Manual"))
        self.label_8.setText(_translate("Cameras", "Crop ROI"))
        self.pushButton_7.setText(_translate("Cameras", "px"))
        self.label_4.setText(_translate("Cameras", "Left"))
        self.pushButton_8.setText(_translate("Cameras", "OFF"))

        def getConfig():
            if os.path.exists("cameras.ini"):
                config = configparser.RawConfigParser()
                config.read("cameras.ini")
                try:
                    if config["Cameras"]["status"] == "off":
                        self.pushButton_10.setChecked(False)
                except Exception as e:
                    pass
                try:
                    if config["Cameras"]["status"] == "on":
                        try:
                            self.horizontalSlider.setValue(config.getint("Exposure", "time"))
                            self.lineEdit.setText(str(config.getint("Exposure", "time")))
                            print("exposure time set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_8.setText(str(config.getint("Delay", "time")))
                            print("delay time set")
                        except Exception as e:
                            pass
                        try:
                            self.comboBox.setCurrentText(config.get("Left camera", "mode"))
                            print("left camera mode set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_2.setText(config.get("Left camera", "lines"))
                            print("left camera lines set")
                        except Exception as e:
                            pass
                        try:
                            self.comboBox_2.setCurrentText(config.get("Right camera", "mode"))
                            print("right camera mode set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_3.setText(config.get("Right camera", "lines"))
                            print("right camera lines set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_4.setText(config.get("ROI", "top"))
                            print("top ROI set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_6.setText(config.get("ROI", "left"))
                            print("left ROI set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_5.setText(config.get("ROI", "width"))
                            print("width ROI set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_7.setText(config.get("ROI", "height"))
                            print("height ROI set")
                        except Exception as e:
                            pass
                    else:
                        pass
                except Exception as e:
                    pass
            else:
                pass
        getConfig()
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Cameras = QtWidgets.QMainWindow()
    ui = Ui_Cameras()
    ui.setupUi(Cameras)
    Cameras.show()
    sys.exit(app.exec_())