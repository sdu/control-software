# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stage.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!

from mqtt import *
from PyQt5 import QtCore, QtWidgets
import os
import configparser
import time
import json
from pynq.overlays.base import BaseOverlay
from pynq.lib import LED, Switch, Button

class Ui_Stage(object):
    def setupUi(self, Stage):
        Stage.setObjectName("Stage")
        Stage.resize(530, 321)
        self.centralwidget = QtWidgets.QWidget(Stage)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.lineEdit_5 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.gridLayout.addWidget(self.lineEdit_5, 5, 2, 1, 1)
        self.lineEdit_5.returnPressed.connect(self.z)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 3, 1, 1)
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.gridLayout.addWidget(self.pushButton_3, 3, 3, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 4, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 2, 1, 1)
        self.lineEdit.returnPressed.connect(self.x)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 3, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 4, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 5, 0, 1, 1)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.gridLayout.addWidget(self.lineEdit_3, 3, 2, 1, 1)
        self.lineEdit_3.returnPressed.connect(self.y)
        self.horizontalSlider_3 = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_3.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_3.setObjectName("horizontalSlider_3")
        self.horizontalSlider_3.setMinimum(0)
        self.horizontalSlider_3.setMaximum(100)
        self.gridLayout.addWidget(self.horizontalSlider_3, 6, 0, 1, 8)
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.gridLayout.addWidget(self.pushButton_5, 5, 3, 1, 1)
        self.lineEdit_7 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.gridLayout.addWidget(self.lineEdit_7, 7, 2, 1, 1)
        self.lineEdit_7.returnPressed.connect(self.r)
        self.horizontalSlider = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")       
        self.horizontalSlider.setMinimum(0)
        self.horizontalSlider.setMaximum(100)
        self.gridLayout.addWidget(self.horizontalSlider, 2, 0, 1, 8)
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setObjectName("pushButton_7")
        self.gridLayout.addWidget(self.pushButton_7, 7, 3, 1, 1)
        self.horizontalSlider_2 = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_2.setObjectName("horizontalSlider_2")
        self.horizontalSlider_2.setMinimum(0)
        self.horizontalSlider_2.setMaximum(100)
        self.gridLayout.addWidget(self.horizontalSlider_2, 4, 0, 1, 8)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 7, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 5, 4, 1, 1)
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setObjectName("pushButton_8")
        self.gridLayout.addWidget(self.pushButton_8, 9, 2, 1, 1)
        self.pushButton_8.clicked.connect(self.counterclockwise)
        self.pushButton_12 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_12.setObjectName("pushButton_12")
        self.gridLayout.addWidget(self.pushButton_12, 9, 6, 1, 1)
        self.pushButton_12.clicked.connect(self.clockwise2)
        self.pushButton_13 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_13.setObjectName("pushButton_13")
        self.gridLayout.addWidget(self.pushButton_13, 9, 7, 1, 1)
        self.pushButton_13.clicked.connect(self.clockwise3)
        self.pushButton_9 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_9.setObjectName("pushButton_9")
        self.gridLayout.addWidget(self.pushButton_9, 9, 3, 1, 1)
        self.pushButton_9.clicked.connect(self.counterclockwise2)
        self.pushButton_11 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_11.setObjectName("pushButton_11")
        self.gridLayout.addWidget(self.pushButton_11, 9, 5, 1, 1)
        self.pushButton_11.clicked.connect(self.clockwise)
        self.horizontalSlider_4 = QtWidgets.QSlider(self.centralwidget)
        self.horizontalSlider_4.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_4.setObjectName("horizontalSlider_4")
        self.gridLayout.addWidget(self.horizontalSlider_4, 8, 0, 1, 8)
        self.pushButton_10 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_10.setObjectName("pushButton_10")
        self.gridLayout.addWidget(self.pushButton_10, 9, 4, 1, 1)
        self.pushButton_10.clicked.connect(self.counterclockwise3)
        self.pushButton_14 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_14.setCheckable(True)
        self.pushButton_14.setChecked(True)
        if not os.path.exists("stage.ini"):
            print("Stage turned on")
            print("LED 0 turning on")
            try:
                base = BaseOverlay("base.bit")
                led0 = base.leds[0]
                led0.on()
            except Exception as e:
                pass

            config = configparser.RawConfigParser()

            f = open("stage.ini", "w")
            config.add_section("Stage")
            config.set("Stage", "status", "on")
            config.write(f)
        else:
            pass
        self.pushButton_14.setObjectName("pushButton_14")
        self.pushButton_14.clicked.connect(self.on)
        self.gridLayout.addWidget(self.pushButton_14, 0, 2, 1, 1)
        self.pushButton_15 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_15.setObjectName("pushButton_15")
        self.gridLayout.addWidget(self.pushButton_15, 0, 3, 1, 1)
        self.pushButton_15.clicked.connect(self.off)
        Stage.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(Stage)
        self.statusbar.setObjectName("statusbar")
        Stage.setStatusBar(self.statusbar)
        self.retranslateUi(Stage)
        QtCore.QMetaObject.connectSlotsByName(Stage)

    def x(self):
        if os.path.exists("stage.ini"):
            textboxValue = self.lineEdit.text()
            if self.lineEdit.text() == "":
                self.horizontalSlider.setValue(0)
                textboxValue = str(0)
            else:
                self.horizontalSlider.setValue(int(textboxValue))
            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
            client.subscribe("microscope/light_sheet_microscope/stage/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
            client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "x position": textboxValue + "um", "cmd": "set x position"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("X position: " + textboxValue + "um")

            config = configparser.RawConfigParser()
            config.read("stage.ini")

            f = open("stage.ini", "w")
            try:
                config.add_section("X")
            except Exception as e:
                pass
            try:    
                config.set("X", "position", textboxValue)
            except Exception as e:
                pass
            config.write(f)           

    def y(self):
        if os.path.exists("stage.ini"):
            textboxValue2 = self.lineEdit_3.text()
            if self.lineEdit_3.text() == "":
                self.horizontalSlider_2.setValue(0)
                textboxValue2 = str(0)
            else:
                self.horizontalSlider_2.setValue(int(textboxValue2))
            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
            client.subscribe("microscope/light_sheet_microscope/stage/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
            client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "y position": textboxValue2 + "um", "cmd": "set y position"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Y position: " + textboxValue2 + "um") 

            config = configparser.RawConfigParser()
            config.read("stage.ini")

            f = open("stage.ini", "w")
            try:
                config.add_section("Y")
            except Exception as e:
                pass
            try:    
                config.set("Y", "position", textboxValue2)
            except Exception as e:
                pass
            config.write(f)     

    def z(self):
        if os.path.exists("stage.ini"):
            textboxValue3 = self.lineEdit_5.text()
            if self.lineEdit_5.text() == "":
                self.horizontalSlider_3.setValue(0)
                textboxValue3 = str(0)
            else:
                self.horizontalSlider_3.setValue(int(textboxValue3))
            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
            client.subscribe("microscope/light_sheet_microscope/stage/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
            client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "z position": textboxValue3 + "um", "cmd": "set z position"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Z position: " + textboxValue3 + "um")

            config = configparser.RawConfigParser()
            config.read("stage.ini")

            f = open("stage.ini", "w")
            try:
                config.add_section("Z")
            except Exception as e:
                pass
            try:    
                config.set("Z", "position", textboxValue3)
            except Exception as e:
                pass
            config.write(f) 

    def r(self):
        if os.path.exists("stage.ini"):
            textboxValue4 = self.lineEdit_7.text()
            if self.lineEdit_7.text() == "":
                self.horizontalSlider_4.setValue(0)
                textboxValue4 = str(0)
            else:
                self.horizontalSlider_4.setValue(int(textboxValue4))
            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
            client.subscribe("microscope/light_sheet_microscope/stage/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
            client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "r": textboxValue4, "cmd": "set r"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("R: " + textboxValue4 + "deg")

            config = configparser.RawConfigParser()
            config.read("stage.ini")

            f = open("stage.ini", "w")
            try:
                config.add_section("R")
            except Exception as e:
                pass
            try:    
                config.set("R", "degrees", textboxValue4)
            except Exception as e:
                pass
            config.write(f) 

    def counterclockwise(self):
        client = device()
        client.run()

        client.loop_start()
        print("\n" + "Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
        client.subscribe("microscope/light_sheet_microscope/stage/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
        client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "90 degrees counter-clockwise"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("90 degrees counterclockwise")

    def counterclockwise2(self):
        client = device()
        client.run()

        client.loop_start()
        print("\n" + "Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
        client.subscribe("microscope/light_sheet_microscope/stage/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
        client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "45 degrees counter-clockwise"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("45 degrees counterclockwise")                    

    def counterclockwise3(self):
        client = device()
        client.run()

        client.loop_start()
        print("\n" + "Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
        client.subscribe("microscope/light_sheet_microscope/stage/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
        client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "15 degrees counter-clockwise"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("15 degrees counterclockwise")

    def clockwise(self):
        client = device()
        client.run()

        client.loop_start()
        print("\n" + "Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
        client.subscribe("microscope/light_sheet_microscope/stage/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
        client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "15 degrees clockwise"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("15 degrees clockwise")

    def clockwise2(self):
        client = device()
        client.run()

        client.loop_start()
        print("\n" + "Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
        client.subscribe("microscope/light_sheet_microscope/stage/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
        client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "45 degrees clockwise"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("45 degrees clockwise")

    def clockwise3(self):
        client = device()
        client.run()

        client.loop_start()
        print("\n" + "Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
        client.subscribe("microscope/light_sheet_microscope/stage/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
        client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "90 degrees clockwise"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("90 degrees clockwise")

    def on(self):
        if os.path.exists("stage.ini"):
            config = configparser.RawConfigParser()
            config.read("stage.ini")

            if self.pushButton_14.isChecked():        
                client = device()
                client.run()

                client.loop_start()
                print("\n" + "Connected to broker")
                time.sleep(1)
                print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
                client.subscribe("microscope/light_sheet_microscope/stage/UI")
                print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
                client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "stage turning on"}}, indent=2))
                time.sleep(1)
                client.loop_stop()
                print("Stage turned on")

                try:
                    self.lineEdit.setText(str(config.getint("X", "position")))
                    print("X position set")
                    self.horizontalSlider.setValue(config.getint("X", "position"))
                    print("X position silider set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_3.setText(str(config.getint("Y", "position")))
                    print("Y position set")
                    self.horizontalSlider_2.setValue(config.getint("Y", "position"))
                    print("Y position slider set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_5.setText(str(config.getint("Z", "position")))
                    print("Z position set")
                    self.horizontalSlider_3.setValue(config.getint("Z", "position"))
                    print("Z position slider set")
                except Exception as e:
                    pass
                try:
                    self.lineEdit_7.setText(str(config.getint("R", "degrees")))
                    print("R degrees set")
                    self.horizontalSlider_4.setValue(config.getint("R", "degrees"))
                    print("R degrees set")
                except Exception as e:
                    pass

                self.pushButton_15.clicked.connect(self.off)
                self.pushButton_14.clicked.connect(self.on)
                self.lineEdit.returnPressed.connect(self.x)
                self.lineEdit_3.returnPressed.connect(self.y)
                self.lineEdit_5.returnPressed.connect(self.z)
                self.lineEdit_7.returnPressed.connect(self.r)
                self.pushButton_8.clicked.connect(self.counterclockwise)
                self.pushButton_9.clicked.connect(self.counterclockwise2)
                self.pushButton_10.clicked.connect(self.counterclockwise3)
                self.pushButton_11.clicked.connect(self.clockwise)
                self.pushButton_12.clicked.connect(self.clockwise2)
                self.pushButton_13.clicked.connect(self.clockwise3)

                f = open("stage.ini", "w")
                try:
                    config.add_section("Stage")
                except Exception as e:
                    pass
                try:
                    config.set("Stage", "status", "on")
                except Exception as e:
                    pass
                config.write(f)

    def off(self):
        if os.path.exists("stage.ini"):
            config = configparser.RawConfigParser()
            config.read("stage.ini")

            client = device()
            client.run()

            client.loop_start()
            print("\n" + "Connected to broker")
            time.sleep(1)
            print("Subscribing to topic", "microscope/light_sheet_microscope/stage/UI")
            client.subscribe("microscope/light_sheet_microscope/stage/UI")
            print("Publishing message to topic", "microscope/light_sheet_microscope/stage/UI")
            client.publish("microscope/light_sheet_microscope/stage/UI", json.dumps({"type": "device", "payload":{"name": "stage", "cmd": "stage turning off"}}, indent=2))
            time.sleep(1)
            client.loop_stop()
            print("Stage turned off")

            self.pushButton_15.clicked.disconnect(self.off)                
            self.pushButton_14.setChecked(False)
            self.lineEdit.returnPressed.disconnect(self.x)
            self.lineEdit.clear()
            self.horizontalSlider.setValue(0)
            self.lineEdit_3.returnPressed.disconnect(self.y)
            self.lineEdit_3.clear()
            self.horizontalSlider_2.setValue(0)
            self.lineEdit_5.returnPressed.disconnect(self.z)
            self.lineEdit_5.clear()
            self.horizontalSlider_3.setValue(0)
            self.lineEdit_7.returnPressed.disconnect(self.r)
            self.lineEdit_7.clear()
            self.horizontalSlider_4.setValue(0)
            self.pushButton_8.clicked.disconnect(self.counterclockwise)
            self.pushButton_9.clicked.disconnect(self.counterclockwise2)
            self.pushButton_10.clicked.disconnect(self.counterclockwise3)
            self.pushButton_11.clicked.disconnect(self.clockwise)
            self.pushButton_12.clicked.disconnect(self.clockwise2)
            self.pushButton_13.clicked.disconnect(self.clockwise3)

            f = open("stage.ini", "w")
            try:
                config.add_section("Stage")
            except Exception as e:
                pass
            try:
                config.set("Stage", "status", "off")
            except Exception as e:
                pass
            config.write(f)  

    def retranslateUi(self, Stage):
        _translate = QtCore.QCoreApplication.translate
        Stage.setWindowTitle(_translate("Stage", "MainWindow"))
        self.pushButton.setText(_translate("Stage", "um"))
        self.pushButton_3.setText(_translate("Stage", "um"))
        self.label.setText(_translate("Stage", "X"))
        self.label_3.setText(_translate("Stage", "Y"))
        self.label_5.setText(_translate("Stage", "Z"))
        self.pushButton_5.setText(_translate("Stage", "um"))
        self.pushButton_7.setText(_translate("Stage", "deg"))
        self.label_7.setText(_translate("Stage", "R"))
        self.pushButton_8.setText(_translate("Stage", "90" + "\n" + "degrees" + "\n" + "counter-clockwise"))
        self.pushButton_12.setText(_translate("Stage", "45" + "\n" + "degrees" + "\n" + "clockwise"))
        self.pushButton_13.setText(_translate("Stage", "90" + "\n" + "degrees" + "\n" + "clockwise"))
        self.pushButton_9.setText(_translate("Stage", "45" + "\n" + "degrees" + "\n" + "clockwise"))
        self.pushButton_11.setText(_translate("Stage", "15" + "\n" + "degrees" + "\n" + "clockwise"))
        self.pushButton_10.setText(_translate("Stage", "15" + "\n" + "degrees" + "\n" + "counter-clockwise"))
        self.pushButton_14.setText(_translate("Stage", "ON"))
        self.pushButton_15.setText(_translate("Stage", "OFF"))

        def getConfig():
            if os.path.exists("stage.ini"):
                config = configparser.RawConfigParser()
                config.read("stage.ini")
                try:
                    if config["Stage"]["status"] == "off":
                        self.pushButton_14.setChecked(False)
                except Exception as e:
                    pass
                try:
                    if config["Stage"]["status"] == "on":
                        try:
                            self.lineEdit.setText(str(config.getint("X", "position")))
                            print("X position set")
                            self.horizontalSlider.setValue(config.getint("X", "position"))
                            print("X position silider set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_3.setText(str(config.getint("Y", "position")))
                            print("Y position set")
                            self.horizontalSlider_2.setValue(config.getint("Y", "position"))
                            print("Y position slider set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_5.setText(str(config.getint("Z", "position")))
                            print("Z position set")
                            self.horizontalSlider_3.setValue(config.getint("Z", "position"))
                            print("Z position slider set")
                        except Exception as e:
                            pass
                        try:
                            self.lineEdit_7.setText(str(config.getint("R", "degrees")))
                            print("R degrees set")
                            self.horizontalSlider_4.setValue(config.getint("R", "degrees"))
                            print("R degrees slider set")
                        except Exception as e:
                            pass
                    else:
                        pass
                except Exception as e:
                    pass
            else:
                pass
        getConfig()
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Stage = QtWidgets.QMainWindow()
    ui = Ui_Stage()
    ui.setupUi(Stage)
    Stage.show()
    sys.exit(app.exec_())