import logging
import paho.mqtt.client as mqtt
import json
from pynq.overlays.base import BaseOverlay
from pynq.lib import LED, Switch, Button
from pynq.lib import Pmod_ADC, Pmod_DAC
import time
import asyncio

class embedded(mqtt.Client):
    def on_connect(self, mqtt, obj, flags, rc):
        pass

    def on_message(self, mqtt, userdata, message):
        asyncio.set_event_loop(asyncio.new_event_loop())
        m_decode = str(message.payload.decode("utf-8"))
        print("\n" + "message recieved= " + m_decode)
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)
        m_in = json.loads(m_decode)

        if message.topic == "microscope/light_sheet_microscope/UI/laser/445nm" and m_in["payload"]["cmd"] == "turning on laser":
            print("Laser LED turning on")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.on()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/445nm" and m_in["payload"]["cmd"] == "turning off laser":
            print("Laser LED turning off")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.off()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/488nm" and m_in["payload"]["cmd"] == "turning on laser":
            print("Laser LED turning on")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.on()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/488nm" and m_in["payload"]["cmd"] == "turning off laser":
            print("Laser LED turning off")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.off()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/515nm" and m_in["payload"]["cmd"] == "turning on laser":
            print("Laser LED turning on")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.on()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/515nm" and m_in["payload"]["cmd"] == "turning off laser":
            print("Laser LED turning off")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.off()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/561nm" and m_in["payload"]["cmd"] == "turning on laser":
            print("Laser LED turning on")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.on()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/561nm" and m_in["payload"]["cmd"] == "turning off laser":
            print("Laser LED turning off")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.off()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/594nm" and m_in["payload"]["cmd"] == "turning on laser":
            print("Laser LED turning on")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.on()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/594nm" and m_in["payload"]["cmd"] == "turning off laser":
            print("Laser LED turning off")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.off()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/638nm" and m_in["payload"]["cmd"] == "turning on laser":
            print("Laser LED turning on")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.on()

        if message.topic == "microscope/light_sheet_microscope/UI/laser/638nm" and m_in["payload"]["cmd"] == "turning off laser":
            print("Laser LED turning off")
            base = BaseOverlay("base.bit")
            led0 = base.leds[0]
            led0.off()              

        if message.topic == "microscope/light_sheet_microscope/UI/laser/445nm" and m_in["payload"]["cmd"] == "set intensity of laser":
            print(m_in["payload"]["voltage"])
            dac = Pmod_DAC(base.PMODA)
            dac.write(m_in["payload"]["voltage"])

        if message.topic == "microscope/light_sheet_microscope/UI/laser/488nm" and m_in["payload"]["cmd"] == "set intensity of laser":
            print(m_in["payload"]["voltage"])
            dac = Pmod_DAC(base.PMODA)
            dac.write(m_in["payload"]["voltage"])

        if message.topic == "microscope/light_sheet_microscope/UI/laser/515nm" and m_in["payload"]["cmd"] == "set intensity of laser":
            print(m_in["payload"]["voltage"])
            dac = Pmod_DAC(base.PMODA)
            dac.write(m_in["payload"]["voltage"])

        if message.topic == "microscope/light_sheet_microscope/UI/laser/561nm" and m_in["payload"]["cmd"] == "set intensity of laser":
            print(m_in["payload"]["voltage"])
            dac = Pmod_DAC(base.PMODA)
            dac.write(m_in["payload"]["voltage"])

        if message.topic == "microscope/light_sheet_microscope/UI/laser/594nm" and m_in["payload"]["cmd"] == "set intensity of laser":
            print(m_in["payload"]["voltage"])           
            dac = Pmod_DAC(base.PMODA)
            dac.write(m_in["payload"]["voltage"])

        if message.topic == "microscope/light_sheet_microscope/UI/laser/638nm" and m_in["payload"]["cmd"] == "set intensity of laser":
            print(m_in["payload"]["voltage"])
            dac = Pmod_DAC(base.PMODA)
            dac.write(m_in["payload"]["voltage"])

        if message.topic == "microscope/light_sheet_microscope" and m_in["cmd"] == "microscope shutting down":
            client.loop_stop()
            client.disconnect()

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)