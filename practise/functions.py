import paho.mqtt.client as paho
import time


def on_subscribe(client, userdata, mid, granted_qos):
    print('Subscribed')

def on_message(client, userdata, msg):
    mybuffer = str(msg.payload)
    print('on_message: '+ mybuffer)
    print(mybuffer)

client = paho.Client()
client.on_subscribe = on_subscribe
client.on_message = on_message
client.connect('test.mosquitto.org')
client.subscribe('$SYS/#')

client.loop_forever()