import logging
from datetime import timedelta
import time
from thespian.actors import *
from transitions import Machine
import paho.mqtt.client as mqtt
import importlib
import os.path
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *   

class device(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            print("Connected to broker") 
        else:
            print("Connection failed")
        
        mqttc.subscribe("microscope/light_sheet_microscope/UI")

    def on_message(self, mqttc, userdata, message):
        msg = str(message.payload.decode("utf-8"))
        print("message received=" ,msg)
        print("message topic=",message.topic)
        print("message qos=",message.qos)
        print("message retain flag=",message.retain)

        #Load the module based on message 
        if os.path.isfile(msg + '.py'):
            print("File exists" + "\n" + msg + " actor created") 
            mod = importlib.import_module(msg)
            # mod.some_function()
            # #Instantiate class 
            # a_class = mod.ClassName()
            # a_class.some_method()

            #Import all from the module into globals 
            module_dict = mod.__dict__
            try:
                to_import = mod.__all__
            except AttributeError:
                to_import = [name for name in module_dict if not name.startswith('_')]
            globals().update({name: module_dict[name] for name in to_import})
        else: 
            print("File " + msg + '.py' +" does not exist!")

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    # def on_subscribe(self, mqttc, obj, mid, granted_qos):
    #     print("Subscribed: "+str(mid)+" "+str(granted_qos))

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setMinimumSize(QSize(300, 200))    
        self.setWindowTitle("PyQt button example - pythonprogramminglanguage.com") 

        pybutton = QPushButton('Click me', self)
        pybutton.clicked.connect(self.clickMethod)
        pybutton.resize(100,32)
        pybutton.move(50, 50)        

    def clickMethod(self):
        pybutton = QPushButton('hi me', self)
        pybutton.show()        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()

print("creating new instance laser")
client = device("Device")
client.run()

client.loop_start() #start the loop
device = "pyqt"
time.sleep(2)
print("Publishing message to topic","microscope/light_sheet_microscope/UI")
client.publish("microscope/light_sheet_microscope/UI",device)
time.sleep(2) # wait
client.loop_stop() #stop the loop
sys.exit( app.exec_())