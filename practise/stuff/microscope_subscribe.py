"""
Book: Hands-On MQTT Programming with Python
Author: Gaston C. Hillar - Twitter.com/gastonhillar
Publisher: Packt Publishing Ltd. - http://www.packtpub.com
"""
from microscope_config import *
import paho.mqtt.client as mqtt
import time
import json


# Publish key is the one that usually starts with the "pub-c-" prefix
# Do not forget to replace the string with your publish key
pubnub_publish_key = ""
# Subscribe key is the one that usually starts with the "sub-c" prefix
# Do not forget to replace the string with your subscribe key
pubnub_subscribe_key = ""
pubnub_mqtt_server_host = "mqtt.pndsn.com"
pubnub_mqtt_server_port = 1883
pubnub_mqtt_keepalive = 60
device_id = microscope_name
pubnub_topic = microscope_name


class Microscope:
    active_instance = None
    def __init__(self, device_id, laser, 
        motorized_mirror_galvo, stages, cameras, fw):
        self.device_id = device_id
        self.laser = laser
        self.motorized_mirror_galvo = motorized_mirror_galvo
        self.stages = stages
        self.cameras = cameras
        self.fw = fw
        self.is_pubnub_connected = False
        Microscope.active_instance = self

    def build_json_message(self):
        # Build a message with the status for the surfboard
        message = {
            "Laser": self.laser,
            "Motorized mirror galvo": self.motorized_mirror_galvo,
            "Stages": self.stages,
            "Cameras": self.cameras,
            "FW": self.fw, 
        }
        json_message = json.dumps(message)
        return json_message


def on_connect_mosquitto(client, userdata, flags, rc):
    print("Result from Mosquitto connect: {}".format(
        mqtt.connack_string(rc)))
    # Check whether the result form connect is the CONNACK_ACCEPTED connack code
    if rc == mqtt.CONNACK_ACCEPTED:
        # Subscribe to a topic filter that provides all the sensors
        sensors_topic_filter = topic_format.format(
            microscope_name,
            "+")
        client.subscribe(sensors_topic_filter, qos=0)


def on_subscribe_mosquitto(client, userdata, mid, granted_qos):
    print("I've subscribed with QoS: {}".format(
        granted_qos[0]))


def print_received_message_mosquitto(msg):
    print("Message received. Topic: {}. Payload: {}".format(
        msg.topic, 
        str(msg.payload)))


def on_laser_message_mosquitto(client, userdata, msg):
    print_received_message_mosquitto(msg)
    Microscope.active_instance.laser = int(msg.payload)


def on_motorized_mirror_galvo_message_mosquitto(client, userdata, msg):
    print_received_message_mosquitto(msg)
    Microscope.active_instance.motorized_mirror_galvo = float(msg.payload)


def on_stages_message_mosquitto(client, userdata, msg):
    print_received_message_mosquitto(msg)
    Microscope.active_instance.stages = float(msg.payload)


def on_cameras_message_mosquitto(client, userdata, msg):
    print_received_message_mosquitto(msg)
    Microscope.active_instance.cameras = float(msg.payload)

def on_fw_message_mosquitto(client, userdata, msg):
    print_received_message_mosquitto(msg)
    Microscope.active_instance.fw = float(msg.payload)

def on_connect_pubnub(client, userdata, flags, rc):
    print("Result from PubNub connect: {}".format(
        mqtt.connack_string(rc)))
    # Check whether the result form connect is the CONNACK_ACCEPTED connack code
    if rc == mqtt.CONNACK_ACCEPTED:
        Microscope.active_instance.is_pubnub_connected = True


def on_disconnect_pubnub(client, userdata, rc):
    Microscope.active_instance.is_pubnub_connected = False
    print("Disconnected from PubNub")

if __name__ == "__main__":
    microscope = Microscope(device_id=device_id,
        laser=0,
        motorized_mirror_galvo=0, 
        stages=0, 
        cameras=0,
        fw=0)
    pubnub_client_id = "{}/{}/{}".format(
        pubnub_publish_key,
        pubnub_subscribe_key,
        device_id)
    pubnub_client = mqtt.Client(client_id=pubnub_client_id,
        protocol=mqtt.MQTTv311)
    pubnub_client.on_connect = on_connect_pubnub
    pubnub_client.on_disconnect = on_disconnect_pubnub
    pubnub_client.connect(host="broker.hivemq.com",
        port=1883,
        keepalive=60)
    pubnub_client.loop_start()
    mosquitto_client = mqtt.Client(protocol=mqtt.MQTTv311)
    mosquitto_client.on_connect = on_connect_mosquitto
    mosquitto_client.on_subscribe = on_subscribe_mosquitto
    mosquitto_client.message_callback_add(
        laser_topic,
        on_laser_message_mosquitto)
    mosquitto_client.message_callback_add(
        motorized_mirror_galvo_topic,
        on_motorized_mirror_galvo_message_mosquitto)
    mosquitto_client.message_callback_add(
        stages_topic,
        on_stages_message_mosquitto)
    mosquitto_client.message_callback_add(
        cameras_topic,
        on_cameras_message_mosquitto)
    mosquitto_client.message_callback_add(
        fw_topic,
        on_fw_message_mosquitto)
    mosquitto_client.connect(host="broker.hivemq.com",
        port=1883,
        keepalive=60)
    mosquitto_client.loop_start()
    try:
        while True:
            if Microscope.active_instance.is_pubnub_connected:
                payload = Microscope.active_instance.build_json_message()
                result = pubnub_client.publish(topic=pubnub_topic,
                    payload=payload,
                    qos=0)
                print("Publishing: {}".format(payload))
            else:
                print("Not connected")
            time.sleep(1)
    except KeyboardInterrupt:
        print("I'll disconnect from both Mosquitto and PubNub")
        pubnub_client.disconnect()
        pubnub_client.loop_stop()
        mosquitto_client.disconnect()
        mosquitto_client.loop_stop()