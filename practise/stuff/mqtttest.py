import paho.mqtt.client as mqtt

broker_url = "broker.hivemq.com"
broker_port = 1883

def on_connect(client, userdata, flags, rc):
	print("Connected with Result Code " (rc))

def on_disconnect(client, userdata, rc):
	print("Client Got Disconnected")

client = mqtt.Client()
client.on_connect = on_connect
client.connect(broker_url, broker_port)

client.publish(topic="TestingTopic", payload="TestingPayload", qos=1, retain=False)
client.subscribe("TestingTopic", qos=1)

client.loop_forever()