#!python3
import paho.mqtt.client as mqtt #import the client1
import time
import logging

broker = "broker.hivemq.com"
port = 1883
logging.basicConfig(level=logging.INFO)
#use DEBUG,INFO,WARNING

def on_log(client, userdata, level, buf):
        logging.info(buf)
def on_connect(client, userdata, flags, rc):
        if rc==0:
                client.connected_flag = True #set flag
                logging.info("connected OK")
        else:
                logging.info("Bad connection returned code="+str(rc))
                client.loop_stop()
def on_disconnect(client, userdata, rc):
        logging.info("client disconnected ok")
def on_publish(client, userdata, mid):
        logging.info("In on_pub callback mid= " + str(mid))
def on_subscribe(client, userdata, mid, granted_qos):
        logging.info("subscribed")
def on_message(client, userdata, message):
        topic = message.topic
        msgr = str(message.payload.decode("utf-8"))
        msgr = "Message Received " + msgr
        logging.info(msgr)

mqtt.Client.connected_flag = False #create flag in class
client = mqtt.Client("python1") #create new instance
client2 = mqtt.Client("python2")
client3 = mqtt.Client("python3")
client4 = mqtt.Client("python4")
client5 = mqtt.Client("python5")
client.on_log = on_log
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_publish = on_publish
client.on_subscribe = on_subscribe
client.on_message = on_message
client.connect(broker, port) #establish connection
client.loop_start()
client2.on_log = on_log
client2.on_connect = on_connect
client2.on_disconnect = on_disconnect
client2.on_publish = on_publish
client2.on_subscribe = on_subscribe
client2.on_message = on_message
client2.connect(broker, port) #establish connection
client2.loop_start()
client3.on_log = on_log
client3.on_connect = on_connect
client3.on_disconnect = on_disconnect
client3.on_publish = on_publish
client3.on_subscribe = on_subscribe
client3.on_message = on_message
client3.connect(broker, port) #establish connection
client3.loop_start()
client4.on_log = on_log
client4.on_connect = on_connect
client4.on_disconnect = on_disconnect
client4.on_publish = on_publish
client4.on_subscribe = on_subscribe
client4.on_message = on_message
client4.connect(broker, port) #establish connection
client4.loop_start()
client5.on_log = on_log
client5.on_connect = on_connect
client5.on_disconnect = on_disconnect
client5.on_publish = on_publish
client5.on_subscribe = on_subscribe
client5.on_message = on_message
client5.connect(broker, port) #establish connection
client5.loop_start()
while not client.connected_flag: #wait in loop
        print("In wait loop")
        time.sleep(1)
time.sleep(3)
logging.info("publishing")
ret = client.publish("micrscope/laser", "Hello World") #publish
logging.info("published return=" + str(ret))
time.sleep(3)
#client.loop()
ret = client2.publish("microscope/motorized mirror galvo", "Hello World") #publish
logging.info("published return=" + str(ret))
time.sleep(3)
#client.loop()
ret = client3.publish("microscope/Stages", "Hello World") #publish
logging.info("published return=" + str(ret))
#client.loop()
time.sleep(3)
ret = client4.publish("microscope/Camera", "Hello World") #publish
logging.info("published return=" + str(ret))
#client.loop()
time.sleep(3)
ret = client5.publish("microscope/FW", "Hello World") #publish
logging.info("published return=" + str(ret))
#client.loop()
time.sleep(3)
#client.loop()
time.sleep(10)
client.loop_stop()
client.disconnect()
client2.loop_stop()
client2.disconnect()
client3.loop_stop()
client3.disconnect()
client4.loop_stop()
client4.disconnect()
client5.loop_stop()
client5.disconnect()