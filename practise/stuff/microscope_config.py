"""
Book: Hands-On MQTT Programming with Python
Author: Gaston C. Hillar - Twitter.com/gastonhillar
Publisher: Packt Publishing Ltd. - http://www.packtpub.com
"""
microscope_name = "light sheet microscope"
topic_format = "microscope/{}/{}"
laser_topic = topic_format.format(
    microscope_name, 
    "laser")
motorized_mirror_galvo_topic = topic_format.format(
    microscope_name, 
    "motorize dmirror galvo")
stages_topic = topic_format.format(
    microscope_name, 
    "stages")
cameras_topic = topic_format.format(
    microscope_name, 
    "cameras")
fw_topic = topic_format.format(
    microscope_name, 
    "fw")