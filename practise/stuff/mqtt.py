import logging
from datetime import timedelta
import time
from thespian.actors import *
from transitions import Machine
import paho.mqtt.client as mqtt
import importlib
import os.path
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets, uic

class device(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            print("Connected to broker")
        else:
            print("Connection failed")

        # mqttc.subscribe("microscope/light_sheet_microscope/UI")
        # print("Subscribing to topic", "microscope/light_sheet_microscope/UI")
        # client.subscribe("microscope/light_sheet_microscope/UI")

    def on_message(self, mqttc, userdata, message):
        msg = str(message.payload.decode("utf-8"))
        print("File which you want to import(with .py extension)")
        print("message recieved= " + msg)
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)