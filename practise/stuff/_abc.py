import datetime
import asyncio
from abc import ABC, abstractmethod
from typing import Optional

class IdentifierABC(ABC):
    @abstractmethod
    def __hash__(self): pass

    @abstractmethod
    def __eq__(self, other): pass

    @abstractmethod
    def __str__(self): pass

    def as_string(self) -> str:
        return str(self)

class IdentifiedABC(ABC):
    """Identified objects"""

    identifier: IdentifierABC

    def has_same_identifier(self, other: "IdentifiedABC") -> bool:
        return other.identifier == self.identifier

    def __hash__(self):
        return hash(self.identifier)

    def __eq__(self, other):
        if isinstance(other, IdentifiedABC):
            return self.has_same_identifier(other)
        return NotImplemented

# Define a coroutine that takes in a future
class ActorABC(IdentifiedABC):
    inbox: "MessageQueueABC"
    reference: "ActorReferenceABC"

    @abstractmethod
    async def stop() -> None:
        print("Event finished")

    # Spin up a quick and simple event loop
    # and run until completed
    loop = asyncio.get_event_loop()
    loop.run_until_complete(stop())

    async def coroutine():
        print('in coroutine')

    event_loop = asyncio.get_event_loop()
    print('starting coroutine')
    coro = coroutine()
    print('entering event loop')
    event_loop.run_until_complete(coro)

class ReferenceABC(IdentifiedABC):
    @abstractmethod
    async def tell(self, message: "MessageABC") -> None: pass

class ActorReferenceABC(ReferenceABC):
    @abstractmethod
    async def tell(self, message: "MessageABC") -> None: pass

    @abstractmethod
    async def ask(self, message: "MessageABC", *, timeout: float = None) -> "MessageABC": pass

class MessageABC(ABC):
    sender: Optional[ReferenceABC]
    creation_date: datetime.datetime

    async def reply(self, message: "MessageABC"):
        await self.sender.tell(message)

class MessageQueueABC(ABC):
    async def put(self, item: MessageABC) -> None: pass

    def put_nowait(self, item: MessageABC) -> None: pass

    async def get(self) -> MessageABC: pass

    def get_nowait(self) -> MessageABC: pass