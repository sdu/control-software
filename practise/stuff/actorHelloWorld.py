import random
import asyncio
from actorio import Actor, Message, DataMessage, ask, EndMainLoop, Reference
from transitions import Machine
import time

class State(object):
    """
    We define a state object which provides some utility functions for the
    individual states within the state machine.
    
    """
    def __init__(self):
        print("Processing current state: ", str(self))

    def on_event(self, event):
        """
        Handle events that are delegated to this State.
        
        """
        pass
    
    def __repr__(self):
        """
        Leverages the __str__ method to describe the State.

        """
        return self.__str__()

    def __str__(self):
        """
        Returns the name of the State.

        """
        return self.__class__.__name__

# Start of our states
class UninitialisedState(State):
    """
    The uninitialised state.        
    
    """
    def on_event(self, event):
        if event == 'Initialised':
            return InitialisedState()

        return self

class InitialisedState(State):
    """
    The initialised state.

    """
    def on_event(self, event):
        if event == "Configured":
            return ConfiguredState()

        return self

class ConfiguredState(State):
    """
    The configured state.

    """
    def on_event(self, event):
        if event == "Running":
            return RunningState()
        return self

class RunningState(State):
    """
    The running state.

    """
    def on_event(self, event):
        if event == "Stop":
            return UninitialisedState()
        return self

class SimpleDevice(object):
    """
    A simple state machine that mimics the functionality of a device from a 
    high level.

    """
    def __init__(self):
        """ Initialise the components. """

        # Start with a default state.
        self.state = UninitialisedState()

    def on_event(self, event):
        """
        This is the bread and butter of the state machine. Incoming events are
        delegated to the given states which then handle the event. The result is
        then assigned as the new state.
        
        """

        # The next state will be the result of the on_event function.
        self.state = self.state.on_event(event)

device = SimpleDevice()

""" Generic device class """
class Device:
    def __init__(self, name, state, configfile, actor):
        self.name = name
        self.state = state
        self.configfile = configfile
        self.actor = actor

    def showDeiveInfo(self):
        print("Name: ", self.name)
        print("State: ", self.state)
        print("Configfile ", self.configfile)
        print("Class :", self.actor)

laser = Device("Laser,", "on/off", "configfile", "Laser")
laser.showDeiveInfo()

""" Shelf class """
class Shelf(dict):

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def __getitem__(self, key):
        return self.__dict__[key]

    def __repr__(self):
        return repr(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    def __delitem__(self, key):
        del self.__dict__[key]

    def clear(self):
        return self.__dict__.clear()

    def copy(self):
        return self.__dict__.copy()

    def has_key(self, k):
        return k in self.__dict__

    def update(self, *args, **kwargs):
        return self.__dict__.update(*args, **kwargs)

    def keys(self):
        return self.__dict__.keys()

    def values(self):
        return self.__dict__.values()

    def items(self):
        return self.__dict__.items()

    def pop(self, *args):
        return self.__dict__.pop(*args)

    def __cmp__(self, dict_):
        return self.__cmp__(self.__dict__, dict_)

    def __contains__(self, item):
        return item in self.__dict__

    def __iter__(self):
        return iter(self.__dict__)

    def __unicode__(self):
        return unicode(repr(self.__dict__))

s = Shelf()
s.shelf = "GFP"
print(s)