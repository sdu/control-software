import logging
from datetime import timedelta
import time
from thespian.actors import *
from transitions import Machine
import paho.mqtt.client as mqtt
import importlib
import os.path
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets, uic

class device(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            print("Connected to broker")
        else:
            print("Connection failed")

        mqttc.subscribe("microscope/light_sheet_microscope/UI")

    def on_message(self, mqttc, userdata, message):
        msg = str(message.payload.decode("utf-8"))
        print("File which you want to import(with .py extension)")
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self,parent = None):
        QMainWindow.__init__(self)
        super(MainWindow, self).__init__(parent)
        self.mdi = QMdiArea()
        self.setCentralWidget(self.mdi)

        self.setMinimumSize(QSize(800, 600))
        self.setWindowTitle("PyQt button example - pythonprogramminglanguage.com")

        pybutton = QPushButton('Add device', self)

        pybutton.clicked.connect(self.importbutton)

        pybutton.move(100, 400)
        pybutton.resize(150, 32)

        self.textbox = QLineEdit(self)
        self.textbox.move(100,350)
        self.textbox.resize(100, 32)
        
        self.fileName_UI = ""

    def importbutton(self):
        self.fileName_UI = self.textbox.text()
        self.loadGUI()

    def getGUIFilename(self):
        return self.fileName_UI

    def loadGUI(self):
        print("Searching file", self.fileName_UI)

        try:
            module = __import__(self.fileName_UI)
            my_class = getattr(module, "SubWindow")

            sub = QMdiSubWindow()

            sub.setWidget(my_class())
            sub.setWindowTitle("New GUI:  " + self.fileName_UI)
            self.mdi.addSubWindow(sub)
            sub.show()

            print("creating new instance " + self.fileName_UI)
            client = device("Device")
            client.run()

            client.loop_start()  # start the loop
            device_message = self.fileName_UI
            time.sleep(2)
            print("Publishing message to topic", "microscope/light_sheet_microscope/UI", " Searching file ", device_message+".py ...")
            client.publish("microscope/light_sheet_microscope/UI", device_message)
            time.sleep(2)  # wait
            client.loop_stop()  # stop the loop
            print("File imported")
        except:
            print("creating new instance " + self.fileName_UI)
            client = device("Device")
            client.run()

            client.loop_start()  # start the loop
            device_message = self.fileName_UI
            time.sleep(2)
            print("Publishing message to topic", "microscope/light_sheet_microscope/UI", " Searching file ", device_message+".py ...")
            client.publish("microscope/light_sheet_microscope/UI", device_message)
            time.sleep(2)  # wait
            client.loop_stop()  # stop the loop
            print(device_message + ".py " + "file doesn't exist")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    publishedMessage = mainWin.getGUIFilename()
    sys.exit(app.exec_())

class State(object):
    """
    We define a state object which provides some utility functions for the
    individual states within the state machine.
    
    """
    def __init__(self):
    	print("Current state: ", str(self))

    def on_event(self, event):
        """
        Handle events that are delegated to this State.
        
        """
        pass
    
    def __repr__(self):
        """
        Leverages the __str__ method to describe the State.

        """
        return self.__str__()

    def __str__(self):
        """
        Returns the name of the State.

        """
        return self.__class__.__name__

# Start of our states
class UninitialisedState(State):
    """
    The uninitialised state.        
    
    """
    def on_event(self, event):
        if event == 'Initialised':
            return InitialisedState()

        return self

class InitialisedState(State):
    """
    The initialised state.

    """
    def on_event(self, event):
        if event == "Configured":
            return ConfiguredState()

        return self

class ConfiguredState(State):
    """
    The configured state.

    """
    def on_event(self, event):
        if event == "Running":
            return RunningState()
        return self

class RunningState(State):
    """
    The running state.

    """
    def on_event(self, event):
        if event == "Stop":
            return UninitialisedState()
        return self

class SimpleDevice(object):
    """
    A simple state machine that mimics the functionality of a device from a 
    high level.

    """
    def __init__(self):
        """ Initialise the components. """

        # Start with a default state.
        self.state = UninitialisedState()

    def on_event(self, event):
        """
        This is the bread and butter of the state machine. Incoming events are
        delegated to the given states which then handle the event. The result is
        then assigned as the new state.
        
        """

        # The next state will be the result of the on_event function.
        self.state = self.state.on_event(event)

device = SimpleDevice()

""" Shelf class """
class Shelf(dict):

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def __getitem__(self, key):
        return self.__dict__[key]

    def __repr__(self):
        return repr(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    def __delitem__(self, key):
        del self.__dict__[key]

    def clear(self):
        return self.__dict__.clear()

    def copy(self):
        return self.__dict__.copy()

    def has_key(self, k):
        return k in self.__dict__

    def update(self, *args, **kwargs):
        return self.__dict__.update(*args, **kwargs)

    def keys(self):
        return self.__dict__.keys()

    def values(self):
        return self.__dict__.values()

    def items(self):
        return self.__dict__.items()

    def pop(self, *args):
        return self.__dict__.pop(*args)

    def __cmp__(self, dict_):
        return self.__cmp__(self.__dict__, dict_)

    def __contains__(self, item):
        return item in self.__dict__

    def __iter__(self):
        return iter(self.__dict__)

    def __unicode__(self):
        return unicode(repr(self.__dict__))

s = Shelf()
s.shelf = "GFP"
print(s)