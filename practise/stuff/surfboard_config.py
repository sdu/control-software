"""
Book: Hands-On MQTT Programming with Python
Author: Gaston C. Hillar - Twitter.com/gastonhillar
Publisher: Packt Publishing Ltd. - http://www.packtpub.com
"""
microscope = "microscope"
topic_format = "microscope/{}/{}"
laser_topic = topic_format.format(
    microscope, 
    "laser 1-6/7")
motorized_mirror_galvo_topic = topic_format.format(
    microscope, 
    "motorized mirror galvo")
stages_topic = topic_format.format(
    microscope, 
    "stages")
Left_and_right_cameras_topic = topic_format.format(
    microscope, 
    "Left and right cameras")
FW_topic = topic_format.format(
    microscope, 
    "FW")