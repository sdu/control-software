import paho.mqtt.client as mqtt
import time
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    print("hi")

class laser(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        print("rc: "+str(rc))

    def on_message(self, mqttc, userdata, message):
        print("message received " ,str(message.payload.decode("utf-8")))
        print("message topic=",message.topic)
        print("message qos=",message.qos)
        print("message retain flag=",message.retain)

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)

print("creating new instance")
client = laser("Laser")
client.run()

client.loop_start() #start the loop
time.sleep(2)
print("Publishing message to topic","microscope/light_sheet_microscope/laser")
client.publish("microscope/light_sheet_microscope/laser","Hello World Im a laser!")
time.sleep(2) # wait
client.loop_stop() #stop the loop

class motorized_mirror_galvo(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        print("rc: "+str(rc))
        print("Subscribing to topic","microscope/light_sheet_microscope/motorized_mirror_galvo")
        mqttc.subscribe("microscope/light_sheet_microscope/motorized_mirror_galvo")

    def on_message(self, mqttc, userdata, message):
        print("message received " ,str(message.payload.decode("utf-8")))
        print("message topic=",message.topic)
        print("message qos=",message.qos)
        print("message retain flag=",message.retain)

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))
    
    # def on_log(self, mqttc, userdata, level, buf):
    #     print("log: ",buf)

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)

print("creating new instance")
client = motorized_mirror_galvo("Motorized mirror galvo")
client.run()

client.loop_start() #start the loop
time.sleep(2)
print("Publishing message to topic","microscope/light_sheet_microscope/motorized_mirror_galvo")
client.publish("microscope/light_sheet_microscope/motorized_mirror_galvo","Hello World Im a motorized mirror galvo!")
time.sleep(2) # wait
client.loop_stop() #stop the loop

class stages(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        print("rc: "+str(rc))
        print("Subscribing to topic","microscope/laser")
        mqttc.subscribe("microscope/light_sheet_microscope/laser")

    def on_message(self, mqttc, userdata, message):
        print("message received " ,str(message.payload.decode("utf-8")))
        print("message topic=",message.topic)
        print("message qos=",message.qos)
        print("message retain flag=",message.retain)

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))
    
    def on_log(self, mqttc, userdata, level, buf):
        print("log: ",buf)

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)

print("creating new instance")
client = stages("Stages")
client.run()

client.loop_start() #start the loop
time.sleep(2)
print("Publishing message to topic","microscope/light_sheet_microscope/stages")
client.publish("microscope/light_sheet_microscope/stages","Hello World Im a stage!")
time.sleep(2) # wait
client.loop_stop() #stop the loop

if __name__ == '__main__':
    app.run(host='0.0.0.0')

# class cameras(mqtt.Client):
#     def on_connect(self, mqttc, obj, flags, rc):
#         print("rc: "+str(rc))
#         print("Subscribing to topic","microscope/cameras")
#         mqttc.subscribe("microscope/light_sheet_microscope/cameras")

#     def on_message(self, mqttc, userdata, message):
#         print("message received " ,str(message.payload.decode("utf-8")))
#         print("message topic=",message.topic)
#         print("message qos=",message.qos)
#         print("message retain flag=",message.retain)

#     def on_publish(self, mqttc, obj, mid):
#         print("mid: "+str(mid))

#     def on_subscribe(self, mqttc, obj, mid, granted_qos):
#         print("Subscribed: "+str(mid)+" "+str(granted_qos))
    
#     def on_log(self, mqttc, userdata, level, buf):
#         print("log: ",buf)

#     def run(self):
#         self.connect("broker.hivemq.com", 1883, 60)

# print("creating new instance")
# client = cameras("Cameras")
# client.run()

# client.loop_start() #start the loop
# time.sleep(2)
# print("Publishing message to topic","microscope/light_sheet_microscope/cameras")
# client.publish("microscope/light_sheet_microscope/cameras","Hello World Im a camera!")
# time.sleep(2) # wait
# client.loop_stop() #stop the loop

# class fw(mqtt.Client):
#     def on_connect(self, mqttc, obj, flags, rc):
#         print("rc: "+str(rc))
#         print("Subscribing to topic","microscope/fw")
#         mqttc.subscribe("microscope/light_sheet_microscope/fw")

#     def on_message(self, mqttc, userdata, message):
#         print("message received " ,str(message.payload.decode("utf-8")))
#         print("message topic=",message.topic)
#         print("message qos=",message.qos)
#         print("message retain flag=",message.retain)

#     def on_publish(self, mqttc, obj, mid):
#         print("mid: "+str(mid))

#     def on_subscribe(self, mqttc, obj, mid, granted_qos):
#         print("Subscribed: "+str(mid)+" "+str(granted_qos))
    
#     def on_log(self, mqttc, userdata, level, buf):
#         print("log: ",buf)

#     def run(self):
#         self.connect("broker.hivemq.com", 1883, 60)

# print("creating new instance")
# client = fw("FW")
# client.run()

# client.loop_start() #start the loop
# time.sleep(2)
# print("Publishing message to topic","microscope/light_sheet_microscope/fw")
# client.publish("microscope/light_sheet_microscope/fw","Hello World Im a FW!")
# time.sleep(2) # wait
# client.loop_stop() #stop the loop