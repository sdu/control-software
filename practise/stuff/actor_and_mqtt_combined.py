import random
import asyncio
from actorio import Actor, Message, DataMessage, ask, EndMainLoop, Reference
import os
os.system("python publish-subscribe2.py")

class Laser(Actor):
    # Here we override the handle_message method to send a `DataMessage` with the data "Hello World!".
    async def handle_message(self, message: Message):
        await message.sender.tell(DataMessage(data="Hello World Im a laser!", sender=self))
async def main():
    # Let's create an instance of a Greeter actor and start it. 
    async with Laser() as laser:
        # Then we'll just send it an empty message and wait for a response
        reply : DataMessage = await ask(laser, Message())
    print(reply.data)
asyncio.get_event_loop().run_until_complete(main())


class Motorized_mirror_galvo(Actor):
    # Here we override the handle_message method to send a `DataMessage` with the data "Hello World!".
    async def handle_message(self, message: Message):
        await message.sender.tell(DataMessage(data="Hello World Im a motorized mirror galvo!", sender=self))
async def main():
    # Let's create an instance of a Greeter actor and start it. 
    async with Motorized_mirror_galvo() as motorized_mirror_galvo:
        # Then we'll just send it an empty message and wait for a response
        reply : DataMessage = await ask(motorized_mirror_galvo, Message())
    print(reply.data)
asyncio.get_event_loop().run_until_complete(main())

class Stages(Actor):
    # Here we override the handle_message method to send a `DataMessage` with the data "Hello World!".
    async def handle_message(self, message: Message):
        await message.sender.tell(DataMessage(data="Hello World Im a stage!", sender=self))
async def main():
    # Let's create an instance of a Greeter actor and start it. 
    async with Stages() as stages:
        # Then we'll just send it an empty message and wait for a response
        reply : DataMessage = await ask(stages, Message())
    print(reply.data)
asyncio.get_event_loop().run_until_complete(main())


class Cameras(Actor):
    # Here we override the handle_message method to send a `DataMessage` with the data "Hello World!".
    async def handle_message(self, message: Message):
        await message.sender.tell(DataMessage(data="Hello World Im a camera!", sender=self))
async def main():
    # Let's create an instance of a Greeter actor and start it. 
    async with Cameras() as cameras:
        # Then we'll just send it an empty message and wait for a response
        reply : DataMessage = await ask(cameras, Message())
    print(reply.data)
asyncio.get_event_loop().run_until_complete(main())


class FW(Actor):
    # Here we override the handle_message method to send a `DataMessage` with the data "Hello World!".
    async def handle_message(self, message: Message):
        await message.sender.tell(DataMessage(data="Hello World Im a FW!", sender=self))
async def main():
    # Let's create an instance of a Greeter actor and start it. 
    async with FW() as fw:
        # Then we'll just send it an empty message and wait for a response
        reply : DataMessage = await ask(fw, Message())
    print(reply.data)
asyncio.get_event_loop().run_until_complete(main())