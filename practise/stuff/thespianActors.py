import logging
from datetime import timedelta
from thespian.actors import *

class Generic_device(Actor):
    def receiveMessage(self, message, sender):
        logging.info('Hello got: %s', message)
        if message == 'Starting laser':
            laser = self.createActor(Laser)
            lasermsg = (sender, 'Starting')
            self.send(laser, lasermsg)

        if message == 'Starting galvo':
            galvo = self.createActor(Galvo)
            galvomsg = (sender, 'Starting')
            self.send(galvo, galvomsg)

        if message == 'Starting cameras':
            cameras = self.createActor(Cameras)
            camerasmsg = (sender, 'Starting')
            self.send(cameras, camerasmsg)

        if message == 'Starting filter wheel':
            filter_wheel = self.createActor(Filter_wheel)
            filter_wheelmsg = (sender, 'Starting')
            self.send(filter_wheel, filter_wheelmsg)

class Laser(Actor):
    def receiveMessage(self, message, sender):
        if isinstance(message, tuple):
            orig_sender, pre_laser = message
            self.send(orig_sender, pre_laser + ' laser')

class Galvo(Actor):
    def receiveMessage(self, message, sender):
        if isinstance(message, tuple):
            orig_sender, pre_galvo = message
            self.send(orig_sender, pre_galvo + ' galvo')

class Cameras(Actor):
    def receiveMessage(self, message, sender):
        if isinstance(message, tuple):
            orig_sender, pre_cameras = message
            self.send(orig_sender, pre_cameras + ' cameras')

class Filter_wheel(Actor):
    def receiveMessage(self, message, sender):
        if isinstance(message, tuple):
            orig_sender, pre_filter_wheel = message
            self.send(orig_sender, pre_filter_wheel + ' filter wheel')

def run_example(systembase=None):
    generic_device = ActorSystem().createActor(Generic_device)
    laser = ActorSystem().ask(generic_device, 'Starting laser', 9)
    print(laser)
    galvo = ActorSystem().ask(generic_device, 'Starting galvo', 1.5)
    print(galvo)
    cameras = ActorSystem().ask(generic_device, 'Starting cameras', 1.5)
    print(cameras)
    filter_wheel = ActorSystem().ask(generic_device, 'Starting filter wheel', 1.5)
    print(filter_wheel)
    ActorSystem().shutdown()

if __name__ == "__main__":
    import sys
    run_example(sys.argv[1] if len(sys.argv) > 1 else None)