"""
Book: Hands-On MQTT Programming with Python
Author: Gaston C. Hillar - Twitter.com/gastonhillar
Publisher: Packt Publishing Ltd. - http://www.packtpub.com
"""
from microscope_config import *
import paho.mqtt.client as mqtt
import time
import csv


def on_connect(client, userdata, flags, rc):
    print("Result from connect: {}".format(
        mqtt.connack_string(rc)))
    # Check whether the result form connect is the CONNACK_ACCEPTED connack code
    if rc != mqtt.CONNACK_ACCEPTED:
        raise IOError("I couldn't establish a connection with the MQTT server")

def publish_value(client, topic, value):
    result = client.publish(topic=topic,
        payload=value,
        qos=0)
    return result


if __name__ == "__main__":
    client = mqtt.Client(protocol=mqtt.MQTTv311)
    client.on_connect = on_connect
    client.connect(host="broker.hivemq.com",
        port=1883,
        keepalive=60)
    client.loop_start()
    publish_debug_message = "{}: {}"
    try:
        laser_value = 1
        motorized_mirror_galvo_value = 1
        stages_value = 1
        cameras_value = 1
        fw_value = 1
        print(publish_debug_message.format(
            laser_topic,
            laser_value))
        print(publish_debug_message.format(
            motorized_mirror_galvo_topic, 
            motorized_mirror_galvo_value))
        print(publish_debug_message.format(
            stages_topic, 
            stages_value))
        print(publish_debug_message.format(
            cameras_topic, 
            cameras_value))
        print(publish_debug_message.format(
            fw_topic, 
            fw_value))                    
        publish_value(client, 
            laser_topic, 
            laser_value)
        publish_value(client, 
            motorized_mirror_galvo_topic, 
            motorized_mirror_galvo_value)
        publish_value(client, 
            stages_topic, 
            stages_value)
        publish_value(client,
            cameras_topic, 
            cameras_value)
        publish_value(client,
            fw_topic, 
            fw_value)
        time.sleep(1)
    except KeyboardInterrupt:
        print("I'll disconnect from the MQTT server")
        client.disconnect()
        client.loop_stop()
