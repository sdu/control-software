import json

new_dictionary = {"hi": "new"}

with open('python_dictionary.json','a') as f:
    str = json.dumps(new_dictionary).replace('{', ',', 1)
    f.seek(0,2)
    f.write(str)