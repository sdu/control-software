import paho.mqtt.client as mqtt
from mqtt2 import *
import os
import time
import json

try:
	os.remove("list_of_devices_currently_active.txt")
	print("Awaiting devices to be activated")
except:
	print("Awaiting devices to be activated")
	
devices = list(map(str,input("Devices to be activated: ").split(",")))

client = device()
client.run()

client.loop_start()
print("Connected to broker")
time.sleep(1)
print("Subscribing to topic", "microscope/light_sheet_microscope/UI")
client.subscribe("microscope/light_sheet_microscope/UI")
print("Publishing message to topic", "microscope/light_sheet_microscope/UI")
client.publish("microscope/light_sheet_microscope/UI", json.dumps({"type": "system", "payload":{"name": devices, "cmd": "activating devices"}}, indent=2))
time.sleep(1)
client.loop_stop()

for item in devices:
	device = (item + "Embedded")
	deviceImport = __import__(device)

with open("list_of_devices_currently_active.txt", "a+") as myfile:
    for item in devices:
        myfile.write(item + "\n")
print("\n" + "List of devices currently active:")

def readFile(fname):
    try:
        with open(fname, "r") as f:
            for item in f:
                print(item.rstrip("\n"))
    except:
        print("No devices added yet")
readFile("list_of_devices_currently_active.txt")