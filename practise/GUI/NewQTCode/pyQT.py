import logging
from datetime import timedelta
import time
from thespian.actors import *
from transitions import Machine
import paho.mqtt.client as mqtt
import importlib
import os.path
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets, uic


class device(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            print("Connected to broker")
        else:
            print("Connection failed")

        mqttc.subscribe("microscope/light_sheet_microscope/UI")

    def on_message(self, mqttc, userdata, message):
        msg = str(message.payload.decode("utf-8"))
        print("File which you want to import(with .py extension)")
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)

    def run(self):
        self.connect("broker.hivemq.com", 1883, 60)



class MainWindow(QtWidgets.QMainWindow):
    def __init__(self,parent = None):
        QMainWindow.__init__(self)
        super(MainWindow, self).__init__(parent)
        self.mdi = QMdiArea()
        self.setCentralWidget(self.mdi)

        self.setMinimumSize(QSize(800, 600))
        self.setWindowTitle("PyQt button example - pythonprogramminglanguage.com")

        pybutton = QPushButton('import', self)

        pybutton.clicked.connect(self.importbutton)

        pybutton.move(100, 400)
        pybutton.resize(100, 32)
        #pybutton1.move(100,105)

        self.textbox = QLineEdit(self)
        self.textbox.move(100,350)
        self.textbox.resize(100, 32)

        self.fileName_UI = ""


    def importbutton(self):
        self.fileName_UI = self.textbox.text()

        self.loadGUI()

    def getGUIFilename(self):
        return self.fileName_UI

    def loadGUI(self):
        print("Searching file", self.fileName_UI)

        #uic.loadUi(self.fileName_UI, self)
        try:
            module = __import__(self.fileName_UI)
            my_class = getattr(module, "SubWindow")

            sub = QMdiSubWindow()



            sub.setWidget(my_class())
            sub.setWindowTitle("New GUI:  " + self.fileName_UI)
            self.mdi.addSubWindow(sub)
            sub.show()


            print("creating new instance laser")
            client = device("Device")
            client.run()

            client.loop_start()  # start the loop
            device_message = self.fileName_UI
            print("Publishing message to topic", "microscope/light_sheet_microscope/UI", " Searching file ", device_message+".py ...")
            client.publish("microscope/light_sheet_microscope/UI", device_message)
            time.sleep(2)  # wait
            client.loop_stop()  # stop the loop

        except:
            print("File doesn't exist")




if __name__ == "__main__":

    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()

    publishedMessage = mainWin.getGUIFilename()

    sys.exit(app.exec_())


