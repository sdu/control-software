from statemachine import StateMachine, State
import time

class TrafficLightMachine(StateMachine):
    "A traffic light machine"
    green = State('Green', initial=True)
    yellow = State('Yellow')
    red = State('Red')

    slowdown = green.to(yellow)
    stop = yellow.to(red)
    go = red.to(green)

    def on_slowdown(self):
        print('Calma, lá!')

    def on_stop(self):
        print('Parou.')

    def on_go(self):
        print('Valendo!')

stm = TrafficLightMachine()
stm.slowdown()
print("hi")
time.sleep(1)
stm.stop()
time.sleep(1)
stm.go()