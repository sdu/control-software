from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import sys

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self,parent = None):
        QMainWindow.__init__(self)
        super(MainWindow, self).__init__(parent)
        self.mdi = QMdiArea()
        self.setCentralWidget(self.mdi)

        self.setMinimumSize(QSize(800, 600))
        self.setWindowTitle("PyQt button example - pythonprogramminglanguage.com")
        
        button = QPushButton("Red button")
        button.setCheckable(True)
        button.setStyleSheet(QString("QPushButton {background-color: red;}"))

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    # publishedMessage = mainWin.getGUIFilename()
    sys.exit(app.exec_())