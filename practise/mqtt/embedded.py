import paho.mqtt.client as mqtt
from mqtt import *
import time
import json

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("broker.hivemq.com",1883,60)

# client.loop_start()
print("Connected to broker")
print("Subscribing to topic", "topic/test")
client.subscribe("topic/test")
client.loop_forever()