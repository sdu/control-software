import paho.mqtt.client as mqtt
from mqtt2 import *
import json
import time

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("broker.hivemq.com",1883,60)

client.loop_start()
time.sleep(1)
print("Subscribing to topic", "topic/test")
client.subscribe("topic/test")
print("Publishing message to topic", "topic/test")
client.publish("topic/test", json.dumps({"msg": "Hello world!"}))
time.sleep(1)
client.loop_stop()