from pynq.overlays.base import BaseOverlay
from pynq.lib import LED, Switch, Button
import time
base = BaseOverlay("base.bit")
led0 = base.leds[0]
for i in range(20):
	led0.toggle()
	time.sleep(.1)