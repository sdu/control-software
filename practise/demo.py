from qt import *
from pyqtconfig import ConfigManager

class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()

        self.setWindowTitle('PyQtConfig Demo')
        self.config = ConfigManager()

        gd = QGridLayout()

        pb = QPushButton("ON")
        pb.setStyleSheet("background-color: green")
        gd.addWidget(pb, 0, 1)

        self.window = QWidget()
        self.window.setLayout(gd)
        self.setCentralWidget(self.window)

    def show_config(self):
        self.current_config_output.setText(str(self.config.as_dict()))

# Create a Qt application
app = QApplication(sys.argv)
app.setOrganizationName("PyQtConfig")
app.setOrganizationDomain("martinfitzpatrick.name")
app.setApplicationName("PyQtConfig")

w = MainWindow()
w.show()
app.exec_()  # Enter Qt application main loop