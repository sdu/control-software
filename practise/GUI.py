import paho.mqtt.client as mqtt
import os
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets, uic
from mqtt2 import *
import json
import time

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self,parent = None):
        QMainWindow.__init__(self)
        super(MainWindow, self).__init__(parent)
        self.mdi = QMdiArea()
        self.setCentralWidget(self.mdi)

        self.setMinimumSize(QSize(800, 600))
        self.setWindowTitle("PyQt button example - pythonprogramminglanguage.com")
        
        client = device()
        client.run()

        client.loop_start()
        print("Connected to broker")
        time.sleep(1)
        print("Subscribing to topic", "microscope/light_sheet_microscope/UI")
        client.subscribe("microscope/light_sheet_microscope/UI")
        print("Publishing message to topic", "microscope/light_sheet_microscope/UI")
        client.publish("microscope/light_sheet_microscope/UI", json.dumps({"type": "system", "payload":{"cmd": "get all devices"}}, indent=2))
        time.sleep(1)
        client.loop_stop()
        print("List of devices currently active:")

        def readFile(fname):
            try:
                with open(fname, "r") as f:
                    for item in f:
                        print(item.rstrip("\n"))
            except:
                print("No devices added yet")
        readFile("list_of_devices_currently_active.txt")        
        
        pybutton = QPushButton('Add device', self)

        pybutton.clicked.connect(self.importbutton)

        pybutton.move(100, 400)
        pybutton.resize(150, 32)

        self.combo = QComboBox(self)        
        self.combo.move(100,350)
        self.combo.resize(100, 32)

        def readFile(fname):
            try:
                with open(fname, "r") as f:
                    for item in f:
                        self.combo.addItem(item)
            except:
                print("No devices active")
        readFile("list_of_devices_currently_active.txt") 
    
    def importbutton(self):
        self.fileName_UI = self.combo.currentText()
        self.loadGUI()

    def loadGUI(self):
        module = __import__(self.fileName_UI.rstrip("\n"))
        my_class = getattr(module, "SubWindow")
        
        sub = QMdiSubWindow()
        
        sub.setWidget(my_class())
        sub.setWindowTitle("New GUI:  " + self.fileName_UI)
        self.mdi.addSubWindow(sub)
        sub.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    # publishedMessage = mainWin.getGUIFilename()
    sys.exit(app.exec_())