from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.slider import Slider

class WidgetContainer(GridLayout):
	def __init__(self, **kwargs):
		super(WidgetContainer, self).__init__(**kwargs)

		self.cols = 2
		self.slider = Slider(min=0, max=100)
		self.textinput = TextInput(text='')
		
		self.add_widget(self.slider)
		self.add_widget(self.textinput)

		self.textinput.bind(text=self.on_value)

	def on_value(self, instance, text):
		self.slider.value = float(instance.text)

class SliderExample(App):
	def build(self):
		widgetContainer = WidgetContainer()
		return widgetContainer

if __name__ == '__main__':
	SliderExample().run()