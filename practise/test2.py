"""Part A:"""

class Beverage:
    def __init__(self, name, cost):
        self.name = name
        self.cost = cost

    def getDescription(self):
        print(self.name)

    def getCost(self):
        print(self.cost)

class RegularCoffee(Beverage):
    def __init__(self, name, cost):
        super().__init__(name, cost)

class DecafCoffee(Beverage):
    def __init__(self, name, cost):
        super().__init__(name, cost)

rc = RegularCoffee("Spencer", 2.2)
rc.getDescription()
rc.getCost()

dc = DecafCoffee("Hi", 9.0)
dc.getDescription()
dc.getCost()

"""Part B:"""

class Beverage:
    def __init__(self, name, cost):
        self.name = name
        self.cost = cost

    def getDescription(self):
        print(self.name)

    def getCost(self):
        print(self.cost)

class RegularCoffee(Beverage):
    def __init__(self, name, cost):
        super().__init__(name, cost)

class DecafCoffee(Beverage):
    def __init__(self, name, cost):
        super().__init__(name, cost)

class Cost(RegularCoffee, DecafCoffee):
    def __init__(self, milk, sugar):
        self.milk = 2
        self.sugar = 2
        # self.cream = 2
        # self.sprinkles = 2
        print(self.milk + self.sugar)
        # print(self.sugar)
        # print(self.cream)
        # print(self.sprinkles)

        # super().__init__(name, cost)

c = Cost(input(2))
# c.getDescription()
# c.getCost()

# class Cost:

# class myStruct():
#     def __init__(self, name, salary, doB, title):
#         self.name = name
#         self.salary = salary
#         self.doB = doB
#         self.title = title

# name = input('Enter name: ')
# salary = input('Enter salary: ')
# doB = input('Enter doB: ')
# title = input('Enter title: ')

# new_object = myStruct(name, salary, doB, title)
